# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Pet(models.Model):
    _name = 'pet.pet'
    _description = 'Pet'

    kind = fields.Char(string="Kind", required=True)
    name = fields.Char(string="Name", required=True)
    breed = fields.Char(string="Breed", required=True)
    age = fields.Integer(string="Age")
    cost = fields.Integer(string="Cost")
    image = fields.Binary(string="Image", attachment=True, store=True)

    pet_id = fields.Many2one('pet.owner', string='Owner')
    asdzzzx22